import os
from enum import Enum
import json
import requests
from requests_toolbelt import MultipartEncoder

from pymessenger import utils

DEFAULT_API_VERSION = 2.6


class NotificationType(Enum):
    regular = "REGULAR"
    silent_push = "SILENT_PUSH"
    no_push = "NO_PUSH"


class Bot:
    def __init__(self, access_token, **kwargs):
        """
            @required:
                access_token
            @optional:
                api_version
                app_secret
        """

        self.api_version = kwargs.get('api_version') or DEFAULT_API_VERSION
        self.app_secret = kwargs.get('app_secret')
        self.graph_url = 'https://graph.facebook.com/v{0}'.format(self.api_version)
        self.access_token = access_token

    @property
    def auth_args(self):
        if not hasattr(self, '_auth_args'):
            auth = {
                'access_token': self.access_token
            }
            if self.app_secret is not None:
                appsecret_proof = utils.generate_appsecret_proof(self.access_token, self.app_secret)
                auth['appsecret_proof'] = appsecret_proof
            self._auth_args = auth
        return self._auth_args

    def send_recipient(self, recipient_id, payload, notification_type=NotificationType.regular):
        payload['recipient'] = {
            'id': recipient_id
        }
        payload['notification_type'] = notification_type.value
        return self.send_raw(payload)

    def send_message(self, recipient_id, message, notification_type=NotificationType.regular,
                     messaging_type='RESPONSE', message_tag=None):
        """
        sends the message to facebook api.
        :param recipient_id:
        :param message:
        :param messaging_type: defaults to RESPONSE
                               UPDATE' -- proactively sent and not in response to a received message
                              'MESSAGE_TAG' -- but tag should be specified
                              refer: https://developers.facebook.com/docs/messenger-platform/send-messages#send_api_basics
        :param message_tag: refer (https://developers.facebook.com/docs/messenger-platform/send-messages/message-tags)
        :param notification_type:
        :return: Response from API as <dict>

        @modified by: Jitendra Saxena
        @modified date: 14/05/2018
        """
        if messaging_type == 'RESPONSE':
            return self.send_recipient(recipient_id, {
                'messaging_type': messaging_type,
                'message': message
            }, notification_type)

        # sends the message with message_tag
        return self.send_recipient(recipient_id, {
            'messaging_type': messaging_type,
            'tag': message_tag,
            'message': message
        }, notification_type)

    # todo make this function work -- not working properly.
    def send_attachment(self, recipient_id, attachment_type, attachment_path,
                        notification_type=NotificationType.regular):
        """Send an attachment to the specified recipient using local path.
        Input:
            recipient_id: recipient id to send to
            attachment_type: type of attachment (image, video, audio, file)
            attachment_path: Path of attachment
        Output:
            Response from API as <dict>
        """
        recipient = {
            'id': recipient_id
        }

        message = {
            'attachment': {
                'type': attachment_type,
                'payload': {}
            }
        }

        payload = {
            'recipient': json.dumps(recipient),
            'notification_type': notification_type,
            'message': json.dumps(message),
            'filedata': (os.path.basename(attachment_path), open(attachment_path, 'rb'))
        }
        multipart_data = MultipartEncoder(payload)
        multipart_header = {
            'Content-Type': multipart_data.content_type
        }
        request_endpoint = '{0}/me/messages'.format(self.graph_url)
        return requests.post(request_endpoint, data=multipart_data,
                             params=self.auth_args, headers=multipart_header).json()

    def send_attachment_url(self, recipient_id, attachment_type, attachment_url,
                            notification_type=NotificationType.regular):
        """Send an attachment to the specified recipient using URL.
        Input:
            recipient_id: recipient id to send to
            attachment_type: type of attachment (image, video, audio, file)
            attachment_url: URL of attachment
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            'attachment': {
                'type': attachment_type,
                'payload': {
                    'url': attachment_url
                }
            }
        }, notification_type)

    def send_text_message(self, recipient_id, message, notification_type=NotificationType.regular):
        """Send text messages to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/text-message
        Input:
            recipient_id: recipient id to send to
            message: message to send
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            'text': message
        }, notification_type)

    def send_generic_message(self, recipient_id, elements, notification_type=NotificationType.regular):
        """Send generic messages to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/generic-template
        Input:
            recipient_id: recipient id to send to
            elements: generic message elements to send
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }
        }, notification_type)

    def send_button_message(self, recipient_id, text, buttons, notification_type=NotificationType.regular):
        """Send text messages to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/button-template
        Input:
            recipient_id: recipient id to send to
            text: text of message to send
            buttons: buttons to send
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": text,
                    "buttons": buttons
                }
            }
        }, notification_type)

    def send_action(self, recipient_id, action, notification_type=NotificationType.regular):
        """Send typing indicators or send read receipts to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/sender-actions

        Input:
            recipient_id: recipient id to send to
            action: action type (mark_seen, typing_on, typing_off)
        Output:
            Response from API as <dict>
        """
        return self.send_recipient(recipient_id, {
            'sender_action': action
        }, notification_type)

    def send_image(self, recipient_id, image_path, notification_type=NotificationType.regular):
        """Send an image to the specified recipient.
        Image must be PNG or JPEG or GIF (more might be supported).
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/image-attachment
        Input:
            recipient_id: recipient id to send to
            image_path: path to image to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment(recipient_id, "image", image_path, notification_type)

    def send_image_url(self, recipient_id, image_url, notification_type=NotificationType.regular):
        """Send an image to specified recipient using URL.
        Image must be PNG or JPEG or GIF (more might be supported).
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/image-attachment
        Input:
            recipient_id: recipient id to send to
            image_url: url of image to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment_url(recipient_id, "image", image_url, notification_type)

    def send_audio(self, recipient_id, audio_path, notification_type=NotificationType.regular):
        """Send audio to the specified recipient.
        Audio must be MP3 or WAV
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/audio-attachment
        Input:
            recipient_id: recipient id to send to
            audio_path: path to audio to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment(recipient_id, "audio", audio_path, notification_type)

    def send_audio_url(self, recipient_id, audio_url, notification_type=NotificationType.regular):
        """Send audio to specified recipient using URL.
        Audio must be MP3 or WAV
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/audio-attachment
        Input:
            recipient_id: recipient id to send to
            audio_url: url of audio to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment_url(recipient_id, "audio", audio_url, notification_type)

    def send_video(self, recipient_id, video_path, notification_type=NotificationType.regular):
        """Send video to the specified recipient.
        Video should be MP4 or MOV, but supports more (https://www.facebook.com/help/218673814818907).
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/video-attachment
        Input:
            recipient_id: recipient id to send to
            video_path: path to video to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment(recipient_id, "video", video_path, notification_type)

    def send_video_url(self, recipient_id, video_url, notification_type=NotificationType.regular):
        """Send video to specified recipient using URL.
        Video should be MP4 or MOV, but supports more (https://www.facebook.com/help/218673814818907).
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/video-attachment
        Input:
            recipient_id: recipient id to send to
            video_url: url of video to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment_url(recipient_id, "video", video_url, notification_type)

    def send_file(self, recipient_id, file_path, notification_type=NotificationType.regular):
        """Send file to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/file-attachment
        Input:
            recipient_id: recipient id to send to
            file_path: path to file to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment(recipient_id, "file", file_path, notification_type)

    def send_file_url(self, recipient_id, file_url, notification_type=NotificationType.regular):
        """Send file to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/file-attachment
        Input:
            recipient_id: recipient id to send to
            file_url: url of file to be sent
        Output:
            Response from API as <dict>
        """
        return self.send_attachment_url(recipient_id, "file", file_url, notification_type)

    def get_user_info(self, recipient_id, fields=None):
        """Getting information about the user
        https://developers.facebook.com/docs/messenger-platform/user-profile
        Input:
          recipient_id: recipient id to send to
        Output:
          Response from API as <dict>
        """
        params = {}
        if fields is not None and isinstance(fields, (list, tuple)):
            params['fields'] = ",".join(fields)

        params.update(self.auth_args)

        request_endpoint = '{0}/{1}'.format(self.graph_url, recipient_id)
        response = requests.get(request_endpoint, params=params)
        if response.status_code == 200:
            return response.json()

        return None

    def send_raw(self, payload):
        request_endpoint = '{0}/me/messages'.format(self.graph_url)
        response = requests.post(
            request_endpoint,
            params=self.auth_args,
            json=payload
        )
        result = response.json()
        return result

    def _send_payload(self, payload):
        """ Deprecated, use send_raw instead """
        return self.send_raw(payload)

    # code jitendra start here
    # Date: 14/05/2018

    def create_attachment(self, attachment_type, media_url=None, attachment_id=None, file_path=None, is_reusable=False):
        """
        creates structure of the attachment depending on the attachment_type.
        :param attachment_type: video|audio|image|file
        :param media_url: url of the media
        :param attachment_id: for sending saved assets.
        :param file_path: for sending media from local path.
        :param is_reusable: set True if attachment needs to be send to other users.
        :return:
        """
        # todo currently doesn't support attachment for uploading from local path.
        if media_url:
            return {
                'type': attachment_type,
                'payload': {
                    'url': media_url,
                    'is_reusable': is_reusable
                }
            }
        elif attachment_id:
            return {
                'type': attachment_type,
                'payload': {
                    'attachment_id': media_url
                }
            }
        # for attaching assets from local path
        else:
            return {
                'type': attachment_type,
                'payload': {
                    'is_reusable': is_reusable
                }
            }

    def send_list_template_message(self, recipient_id, elements, buttons, notification_type=NotificationType.regular):
        """Send list-template messages to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-messages/template/list
        Input:
            recipient_id: recipient id to send to
            elements: list-template elements to send
            buttons: (optional) button (array) to display at end of the list templates for postback.\
                     Maximum of one is supported.
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "list",
                    "top_element_style": "compact",
                    "elements": elements,
                    "buttons": buttons
                }
            }
        }, notification_type)

    def send_media_template_message(self, recipient_id, elements, notification_type=NotificationType.regular):
        """Send media-template messages to the specified recipient.
        set media_type to video|image
        either use attachment_id or url inside elements.
        https://developers.facebook.com/docs/messenger-platform/send-messages/template/media
        Input:
            recipient_id: recipient id to send to
            elements: media-template elements to send
        Output:
            Response from API as <dict>
        """
        return self.send_message(recipient_id, {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "media",
                    "elements": elements,
                }
            }
        }, notification_type)

    def send_quick_replies_message(self, recipient_id, text, quick_replies, attachment_data=None,
                                   notification_type=NotificationType.regular):
        """
        Send quick-replies to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/reference/send-api/quick-replies
        Either text or attachment should be set.
        Input:
            recipient_id: recipient id to send to
            text: Non-empty message text to send with the quick replies.
            attachment_type: (optional)
            quick_replies: An array of objects the describe the quick reply buttons to send.
                           A maximum of 11 quick replies are supported.
        Output:
            Response from API as <dict>
        """
        # todo implement support for attachment if loaded from local path.
        if not attachment_data:
            return self.send_message(recipient_id, {
                "text": text,
                "quick_replies": quick_replies
            }, notification_type)

        return self.send_message(recipient_id, {
            "attachment": attachment_data,
            "quick_replies": quick_replies
        }, notification_type)

    def send_persistent_menu_message(self, menu_items, locale='default', composer_input_disabled=False):
        """
        sets a persistent menu to messenger profile.
        https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/persistent-menu
        For creating nested persistent menus, refer:
        ==> https://developers.facebook.com/docs/messenger-platform/send-messages/persistent-menu#nested
        Input:
            menu_items: (array) of menu_items --> web_url, postback, nested
            locale: set the locale.
            composer_input_disabled: set to true to make the persistent menu the only way for a person to
                                     interact with your messenger bot.
        Output:
            Response from API as <dict>
        """
        persistent_menu = {
            "persistent_menu": [
                {
                    "locale": locale,
                    "composer_input_disabled": composer_input_disabled,
                    "call_to_actions": menu_items
                }
            ]
        }

        request_endpoint = '{0}/me/messenger_profile'.format(self.graph_url)
        response = requests.post(
            request_endpoint,
            params=self.auth_args,
            json=persistent_menu
        )
        result = response.json()
        return result

    def upload_attachment(self, attachment_type, attachment_path, is_reusable=True):
        """Uploads an attachment using local path.
        Input:
            attachment_type: type of attachment (image, video, audio, file)
            attachment_path: Path of attachment
            is_reusable: set to True for sending it other recipients
        Output:
            Response from API as <dict> containing attachment_id
        """
        message = {
            'attachment': {
                'type': attachment_type,
                'payload': {
                    "is_reusable": is_reusable
                }
            }
        }
        message = json.dumps(message)
        payload = {
            'message': message,
            'filedata': (attachment_path, open(attachment_path, 'rb'), 'image/png')
        }
        multipart_data = MultipartEncoder(payload)
        multipart_header = {
            'Content-Type': multipart_data.content_type
        }

        request_endpoint = '{0}/me/message_attachments'.format(self.graph_url)
        return requests.post(request_endpoint, data=multipart_data,
                             params=self.auth_args, headers=multipart_header).json()

    def upload_attachment_url(self, attachment_type, attachment_url, is_reusable=True):
        """Uploads an attachment using URL.
        Input:
            attachment_type: type of attachment (image, video, audio, file)
            attachment_url: URL of attachment
            is_reusable: set to True for sending it other recipients
        Output:
            Response from API as <dict> containing attachment_id
        """
        payload = {
            'message': {
                'attachment': {
                    'type': attachment_type,
                    'payload': {
                        "is_reusable": is_reusable,
                        "url": attachment_url
                    }
                }

            },
        }

        request_endpoint = '{0}/me/message_attachments'.format(self.graph_url)
        response = requests.post(
            request_endpoint,
            params=self.auth_args,
            json=payload
        )
        result = response.json()
        return result
