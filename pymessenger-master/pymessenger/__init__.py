import json
import six

from .bot import Bot


class Element(dict):
    __acceptable_keys = ['title', 'item_url', 'image_url', 'subtitle', 'buttons', 'media_type', 'url',
                         'attachment_id', 'default_action']

    def __init__(self, *args, **kwargs):
        if six.PY2:
            kwargs = {k: v for k, v in kwargs.iteritems() if k in self.__acceptable_keys}
        else:
            kwargs = {k: v for k, v in kwargs.items() if k in self.__acceptable_keys}
        super(Element, self).__init__(*args, **kwargs)

    def to_json(self):
        return json.dumps({k: v for k, v in self.iteritems() if k in self.__acceptable_keys})


class Button(dict):
    # TODO: Decide if this should do more
    # refer https://developers.facebook.com/docs/messenger-platform/send-messages/buttons
    # for creating different types of buttons.
    pass


class QuickReplies(dict):
    """
    @developer: Jitendra Saxena
    @created_date: 14/05/2018

    content_type: text | location | user_phone_number | user_email
    title: (optional) text to display on quick_reply button (20 characters limit)
           (required) if content-type is text.
    payload: (string | number) must be set if content-type is text.
    image_url: (optional) but required if title is empty string.
    """
    __acceptable_keys = ['content_type', 'title', 'payload', 'image_url']

    def __init__(self, *args, **kwargs):
        if six.PY2:
            kwargs = {k: v for k, v in kwargs.iteritems() if k in self.__acceptable_keys}
        else:
            kwargs = {k: v for k, v in kwargs.items() if k in self.__acceptable_keys}
        super(Element, self).__init__(*args, **kwargs)

    def to_json(self):
        return json.dumps({k: v for k, v in self.iteritems() if k in self.__acceptable_keys})


class MenuItem(dict):
    """
    @developer: Jitendra Saxena
    @created_date: 14/05/2018
    refer: https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/persistent-menu#menu_item

    type: value of menu item -> web_url, nested, postback
    title: to display on the menu item (30 chars limit).
    url: url to open when button is tapped (required if type is web_url)
    payload: (string) must be set if type is postback (1000 char limit).
    call_to_actions: array(menu_item) -- forms next level menu.
    webview_height_ratio: (optional) String: compact, tall, full
    messenger_extensions: (optional) true if type is web_url
    fallback_url: (optional)
    webview_share_button: (optional) set to 'hide' to stop sharing in webview.
    """
    __acceptable_keys = ['type', 'title', 'url', 'payload', 'call_to_actions', 'webview_height_ratio',
                         'messenger_extensions', 'fallback_url', 'webview_share_button']

    def __init__(self, *args, **kwargs):
        if six.PY2:
            kwargs = {k: v for k, v in kwargs.iteritems() if k in self.__acceptable_keys}
        else:
            kwargs = {k: v for k, v in kwargs.items() if k in self.__acceptable_keys}
        super(Element, self).__init__(*args, **kwargs)

    def to_json(self):
        return json.dumps({k: v for k, v in self.iteritems() if k in self.__acceptable_keys})
