import json
import os
from .utils import create_signature_string, unsign_signature_string
from .settings import CHAT_BOT_VERIFY_TOKEN, FB_BOT
from django.views.decorators.csrf import csrf_exempt
from django.http import *
from pymessenger import Button, Element
from django.http import FileResponse
import mimetypes


def get_buttons():
    b1 = Button(title='Arsenal', type='web_url', url='http://arsenal.com')
    b2 = Button(title='Other', type='postback', payload='hiee')
    buttons = [b1, b2]
    return buttons


@csrf_exempt
def get_medial_files(request):

    if request.method == 'GET':
        data = request.GET.get('data')
        resource_name = unsign_signature_string(data, 10000)
        fullpath = os.path.join('static', resource_name)
        content_type, encoding = mimetypes.guess_type(fullpath)
        content_type = content_type or 'application/octet-stream'
        response = FileResponse(open(fullpath, 'rb'), content_type=content_type)

        if encoding:
            response["Content-Encoding"] = encoding
        return response


@csrf_exempt
def facebook_webhook(request):
    """
    handles the get and post request for the chatbot.
    :param request:
    :return:
    """
    if request.method == 'GET':
        mode = request.GET.get('hub.mode')
        token = request.GET.get('hub.verify_token')
        challenge = request.GET.get('hub.challenge')

        if mode and token:
            if mode == 'subscribe' and token == CHAT_BOT_VERIFY_TOKEN:
                return HttpResponse(challenge)

        return HttpResponseForbidden('Token verificatoin failed')

    if request.method == 'POST':
        body = json.loads(request.body)

        if body['object'] == 'page':
            for event in body['entry']:
                messaging = event['messaging']
                # print(messaging)
                for x in messaging:
                    if x.get('message'):
                        recipient_id = x['sender']['id']
                        if x['message'].get('text'):
                            message = create_signature_string('arsenal.png')
                            FB_BOT.send_text_message(recipient_id, message)
                            # print(unsign_signature_string(message, 4))
                        if x['message'].get('attachments'):
                            for att in x['message'].get('attachments'):
                                buttons = get_buttons()
                                # buttons = Button(title='Other', type='postback', payload='hiee')
                                # image_path = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPKmLcOEtkm4JN0ztE-HT8yzH1v4Dn2f6Plu0e7nXQZFlEBS6SRA'
                                # image_path = 'static/arsenal.png' + ';type=image/png'
                                image_path = 'static/arsenal.png'
                                attachment_id = '251454638755054'
                                result = FB_BOT.upload_attachment('image', image_path, False)
                                print(result)
                                # buttons = [buttons]
                                # element = Element(media_type="image", attachment_id=attachment_id,
                                #                   buttons=buttons)
                                # elements = [element]
                                # print(elements)
                                # result = FB_BOT.send_media_template_message(recipient_id, elements)
                                # print(result)
                                # FB_BOT.send_button_message(recipient_id, 'what do you want me to do', buttons)
                                # FB_BOT.send_list_template_message(recipient_id, elements, buttons)
                    else:
                        pass

        return HttpResponse('OK')
