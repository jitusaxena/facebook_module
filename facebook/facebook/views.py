import json
import os
from .utils import create_signature_string, unsign_signature_string
from .settings import CHAT_BOT_VERIFY_TOKEN, FB_BOT, DYNAMIC_URLS
from django.views.decorators.csrf import csrf_exempt
from django.http import *
from pymessenger import Button, Element
from django.http import FileResponse
import mimetypes


def get_buttons():
    b1 = Button(title='Arsenal', type='web_url', url='http://arsenal.com')
    b2 = Button(title='Other', type='postback', payload='hiee')
    buttons = [b1, b2]
    return buttons


@csrf_exempt
def get_medial_files(request):

    if request.method == 'GET':
        data = request.GET.get('data')
        if data in DYNAMIC_URLS:
            DYNAMIC_URLS.remove(data)
            resource_name = unsign_signature_string(data, 10000)
            fullpath = os.path.join('static', resource_name)
            content_type, encoding = mimetypes.guess_type(fullpath)
            content_type = content_type or 'application/octet-stream'
            response = FileResponse(open(fullpath, 'rb'), content_type=content_type)

            if encoding:
                response["Content-Encoding"] = encoding

            return response
        return HttpResponseNotFound("Not available")


@csrf_exempt
def facebook_webhook(request):
    """
    handles the get and post request for the chatbot.
    :param request:
    :return:
    """
    if request.method == 'GET':
        mode = request.GET.get('hub.mode')
        token = request.GET.get('hub.verify_token')
        challenge = request.GET.get('hub.challenge')

        if mode and token:
            if mode == 'subscribe' and token == CHAT_BOT_VERIFY_TOKEN:
                return HttpResponse(challenge)

        return HttpResponseForbidden('Token verificatoin failed')

    if request.method == 'POST':
        body = json.loads(request.body)

        if body['object'] == 'page':
            for event in body['entry']:
                messaging = event['messaging']
                print(messaging)
                for x in messaging:
                    if x.get('message'):
                        recipient_id = x['sender']['id']
                        if x['message'].get('text'):
                            message = create_signature_string('arsenal.png')
                            FB_BOT.send_text_message(recipient_id, message)
                            # print(unsign_signature_string(message, 4))
                        if x['message'].get('attachments'):
                            for att in x['message'].get('attachments'):
                                buttons = get_buttons()
                                image_name = 'parrot.jpg'
                                signature_url = create_signature_string(image_name)
                                DYNAMIC_URLS.append(signature_url)
                                image_url = 'https://dev4.decisionpoint.in/files?data=' + signature_url
                                # image_url = 'http://thepublicvoice.org/wordpress/wp-content/uploads/2016/01/logoTPV-300x97.jpg'
                                # image_url = 'https://salesreporteruat.decisionpoint.in/media/all.png'
                                # image_url = 'https://0ad8fcea.ngrok.io/static/parrot.jpg'
                                element = Element(title="Arsenal", image_url=image_url, subtitle='More info',
                                                  buttons=buttons)
                                elements = [element, element, element]
                                # print(elements)
                                result = FB_BOT.send_generic_message(recipient_id, elements)
                                print(result, image_url)
                                # FB_BOT.send_button_message(recipient_id, 'what do you want me to do', buttons)
                                # FB_BOT.send_list_template_message(recipient_id, elements, buttons)
                    else:
                        pass

        return HttpResponse('OK')
