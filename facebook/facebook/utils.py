from django.core import signing
from rest_framework import exceptions

time_stamp_signer = signing.TimestampSigner()


def create_signature_string(resource_name):
    """
    Creates a signed url for providing access to protected resources.
    :param resource_name:
    :return: None
    """
    data = {
        'resource': resource_name
    }
    signed_string = time_stamp_signer.sign(signing.dumps(data))

    # create public url
    return signed_string


def unsign_signature_string(singature_string, max_age=60):
    """

    :param singature_string: string containing signed data.
    :param max_age: time in seconds - the url is valid.
    :return: original data
    """
    try:
        data = signing.loads(time_stamp_signer.unsign(singature_string, max_age))
    except signing.SignatureExpired:
        raise exceptions.PermissionDenied()
    except signing.BadSignature:
        raise exceptions.PermissionDenied()
    return data['resource']

